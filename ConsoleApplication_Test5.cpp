﻿#include <iostream>
#include <math.h>
using namespace std;

class Vector
{
private:

    double x;
    double y;
    double z;
    
public:

    Vector (): x(0), y(0), z(0)
    {}

    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}
    
    double XYZ()
    {
        cout << "Введите x: ";
        cin >> x ;
        cout << "\nВведите y: ";
        cin >> y;
        cout << "\nВведите z: ";
        cin >> z;
        return (x, y, z);
    }
    double mod()
    {
        return sqrt(x * x + y * y + z * z);
    }

    void Show()
    {
        cout << "\nx=" << x << " y=" << y << " z=" << z << endl;
    }
};

void main()
{
    setlocale(LC_ALL, "Russian");
   
    Vector A;
    A.XYZ();
    A.Show();
    cout << "Модуль вектора = " << A.mod() << endl;
} 